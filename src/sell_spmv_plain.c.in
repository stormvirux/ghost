/*!GHOST_AUTOGEN CHUNKHEIGHT;BLOCKDIM1 */
#include "ghost/config.h"
#include "ghost/types.h"
#include "ghost/util.h"
#include "ghost/instr.h"
#include "ghost/omp.h"
#include "ghost/machine.h"
#include "ghost/math.h"
#include "ghost/sparsemat.h"
#include "ghost/sell_spmv_plain_gen.h"

#GHOST_SUBST NVECS ${BLOCKDIM1}
#GHOST_SUBST CHUNKHEIGHT ${CHUNKHEIGHT}

ghost_error ghost_sellspmv__u_plain_d_d_rm_CHUNKHEIGHT_NVECS(ghost_densemat *res, ghost_sparsemat *mat, ghost_densemat* invec, ghost_spmv_opts traits)
{
    GHOST_FUNC_ENTER(GHOST_FUNCTYPE_MATH|GHOST_FUNCTYPE_KERNEL);

    double *mval = (double *)SELL(mat)->val;
    double *local_dot_product = NULL;
    double *partsums = NULL;
    int nthreads = 1, i;
    ghost_lidx v;
   
    double *shift = NULL, scale = 1., beta = 1.; 
    double delta = 0., eta = 0.;
    ghost_densemat *z = NULL;
    unsigned clsize;
    ghost_machine_cacheline_size(&clsize);
    int pad = (int) clsize/sizeof(double);

    GHOST_SPMV_PARSE_TRAITS(traits,scale,beta,shift,local_dot_product,z,delta,eta,double,
            double);

    if (traits.flags & GHOST_SPMV_DOT) {
#pragma omp parallel
        nthreads = ghost_omp_nthread();

        GHOST_CALL_RETURN(ghost_malloc((void **)&partsums,
                    (3*res->traits.ncols+pad)*nthreads*sizeof(double))); 
        for (i=0; i<(3*res->traits.ncols+pad)*nthreads; i++) {
            partsums[i] = 0.;
        }
    }
   
#pragma omp parallel shared (partsums) private(v)
    {
        ghost_lidx j,c,r,row;
        ghost_lidx offs;
        double tmp[CHUNKHEIGHT][NVECS];
        double *rhsval = (double *)invec->val;
        double *zval = NULL;
        if (z) {
            zval = (double *)z->val;
        }
        int tid = ghost_omp_threadnum();

#pragma omp for schedule(runtime)
        for (c=0; c<mat->nrowsPadded/CHUNKHEIGHT; c++) 
        { // loop over chunks
            offs = SELL(mat)->chunkStart[c];
                    
            memset(tmp,0,sizeof(tmp));
            for (j=0; j<SELL(mat)->chunkLen[c]; j++) { // loop inside chunk
                for (r=0; r<CHUNKHEIGHT; r++) {
                    double matval = mval[offs+j*CHUNKHEIGHT+r];
                    row = c*CHUNKHEIGHT+r;
                    ghost_lidx matcol = SELL(mat)->col[offs+j*CHUNKHEIGHT+r];
#pragma simd
                    for (v=0; v<NVECS; v++) {
                        tmp[r][v] += matval*rhsval[invec->stride*matcol+v];
                    }
                }
            }

            for (r=0; (r<CHUNKHEIGHT) && (c*CHUNKHEIGHT+r < mat->nrows); r++) {
                row = c*CHUNKHEIGHT+r;
                double *lrow = ((double *)(res->val))+res->stride*row;
                double *rrow = rhsval+invec->stride*row;
                double *zrow = NULL;
                if (z) {
                    zrow = zval+z->stride*row;
                }
                if ((traits.flags & GHOST_SPMV_SHIFT) && shift) {
#if NVECS > 1
#pragma simd
#endif
                    for (v=0; v<NVECS; v++) {
                        tmp[r][v] = tmp[r][v]-shift[0]*rrow[v];
                    }
                }
                if ((traits.flags & GHOST_SPMV_VSHIFT) && shift) {
#if NVECS > 1
#pragma simd
#endif
                    for (v=0; v<NVECS; v++) {
                        tmp[r][v] = tmp[r][v]-shift[v]*rrow[v];
                    }
                }
                if (traits.flags & GHOST_SPMV_SCALE) {
#if NVECS > 1
#pragma simd
#endif
                    for (v=0; v<NVECS; v++) {
                        tmp[r][v] = tmp[r][v]*scale;
                    }
                }
                if (traits.flags & GHOST_SPMV_AXPY) {
#if NVECS > 1
#pragma simd
#endif
                    for (v=0; v<NVECS; v++) {
                        lrow[v] += tmp[r][v];
                    }
                } else if (traits.flags & GHOST_SPMV_AXPBY) {
#if NVECS > 1
#pragma simd
#endif
                    for (v=0; v<NVECS; v++) {
                        lrow[v] = beta*lrow[v] + tmp[r][v];
                    }
                } else {
                    if (traits.flags & GHOST_SPMV_CHAIN_AXPBY) {
#if NVECS > 1
#pragma simd
#endif
                        for (v=0; v<NVECS; v++) {
                            lrow[v] = tmp[r][v];
                        }
                    } else {
#pragma vector nontemporal
#if NVECS > 1
#pragma simd
#endif
                        for (v=0; v<NVECS; v++) {
                            lrow[v] = tmp[r][v];
                        }
                    }
                }
                if (traits.flags & GHOST_SPMV_CHAIN_AXPBY) {
#if NVECS > 1
#pragma simd
#endif
                    for (v=0; v<NVECS; v++) {
                        zrow[v] = delta*zrow[v] + eta*lrow[v];
                    }
                }
                    
                if (traits.flags & GHOST_SPMV_DOT) {
#if NVECS > 1
#pragma simd
#endif
                    for (v=0; v<NVECS; v++) {
                        partsums[((pad+3*res->traits.ncols)*tid)+3*v+0] += 
                            lrow[v]*lrow[v];
                        partsums[((pad+3*res->traits.ncols)*tid)+3*v+1] += 
                            lrow[v]*rrow[v];
                        partsums[((pad+3*res->traits.ncols)*tid)+3*v+2] += 
                            rrow[v]*rrow[v];
                    }
                }
            }
        }
    }
    if (traits.flags & GHOST_SPMV_DOT) {
        if (!local_dot_product) {
            ERROR_LOG("The location of the local dot products is NULL!");
            return GHOST_ERR_INVALID_ARG;
        }
        for (v=0; v<res->traits.ncols; v++) {
            local_dot_product[v                       ] = 0.; 
            local_dot_product[v  +   res->traits.ncols] = 0.;
            local_dot_product[v  + 2*res->traits.ncols] = 0.;
            for (i=0; i<nthreads; i++) {
                local_dot_product[v                      ] += 
                    partsums[(pad+3*res->traits.ncols)*i + 3*v + 0];
                local_dot_product[v +   res->traits.ncols] += 
                    partsums[(pad+3*res->traits.ncols)*i + 3*v + 1];
                local_dot_product[v + 2*res->traits.ncols] += 
                    partsums[(pad+3*res->traits.ncols)*i + 3*v + 2];
            }
        }
        free(partsums);
    }

    GHOST_FUNC_EXIT(GHOST_FUNCTYPE_MATH|GHOST_FUNCTYPE_KERNEL);
    return GHOST_SUCCESS;
}

ghost_error ghost_sellspmv__a_plain_d_d_cm_CHUNKHEIGHT_NVECS(ghost_densemat *res, ghost_sparsemat *mat, ghost_densemat* invec, ghost_spmv_opts traits)
{
    GHOST_FUNC_ENTER(GHOST_FUNCTYPE_MATH|GHOST_FUNCTYPE_KERNEL);

    double *mval = (double *)SELL(mat)->val;
    double *local_dot_product = NULL;
    double *partsums = NULL;
    int nthreads = 1, i;
    ghost_lidx v;
   
    double *shift = NULL, scale = 1., beta = 1.; 
    double delta = 0., eta = 0.;
    ghost_densemat *z = NULL;
    unsigned clsize;
    ghost_machine_cacheline_size(&clsize);
    int pad = (int) clsize/sizeof(double);

    GHOST_SPMV_PARSE_TRAITS(traits,scale,beta,shift,local_dot_product,z,delta,eta,double,
            double);

    if (traits.flags & GHOST_SPMV_DOT) {
#pragma omp parallel
        nthreads = ghost_omp_nthread();

        GHOST_CALL_RETURN(ghost_malloc((void **)&partsums,
                    (3*res->traits.ncols+pad)*nthreads*sizeof(double))); 
        for (i=0; i<(3*res->traits.ncols+pad)*nthreads; i++) {
            partsums[i] = 0.;
        }
    }
   
#pragma omp parallel shared (partsums) private(v)
    {
        ghost_lidx j,c,r,row;
        ghost_lidx offs;
        double tmp[CHUNKHEIGHT];
        double *rhsval = (double *)invec->val;
        int tid = ghost_omp_threadnum();

#pragma omp for schedule(runtime)
        for (c=0; c<mat->nrowsPadded/CHUNKHEIGHT; c++) 
        { // loop over chunks
            offs = SELL(mat)->chunkStart[c];
            
            for (v=0; v<NVECS; v++) {
                memset(tmp,0,sizeof(tmp));
                double *lcol = ((double *)(res->val))+res->stride*v;
                double *rcol = rhsval+invec->stride*v;
                double *zcol = NULL;
                if (z) { 
                    zcol = (double *)z->val+z->stride*v;
                }

                for (j=0; j<SELL(mat)->chunkLen[c]; j++) { // loop inside chunk
#pragma simd
                    for (r=0; r<CHUNKHEIGHT; r++) {
                        double matval = mval[offs+j*CHUNKHEIGHT+r];
                        ghost_lidx matcol = SELL(mat)->col[offs+j*CHUNKHEIGHT+r];
                        tmp[r] += matval*rcol[matcol];
                    }
                }

                for (r=0; (r<CHUNKHEIGHT) && (c*CHUNKHEIGHT+r < mat->nrows); r++) {
                    row = c*CHUNKHEIGHT+r;
                    
                    if ((traits.flags & GHOST_SPMV_SHIFT) && shift) {
                        tmp[r] = tmp[r]-shift[0]*rcol[row];
                    }
                    if ((traits.flags & GHOST_SPMV_VSHIFT) && shift) {
                        tmp[r] = tmp[r]-shift[v]*rcol[row];
                    }
                    if (traits.flags & GHOST_SPMV_SCALE) {
                        tmp[r] = tmp[r]*scale;
                    }
                    if (traits.flags & GHOST_SPMV_AXPY) {
                        lcol[row] += tmp[r];
                    } else if (traits.flags & GHOST_SPMV_AXPBY) {
                        lcol[row] = beta*lcol[row] + tmp[r];
                    } else {
                        lcol[row] = tmp[r];
                    }
                    if (traits.flags & GHOST_SPMV_CHAIN_AXPBY) {
                        zcol[row] = delta*zcol[row] + eta*lcol[row];
                    }
                    
                    if (traits.flags & GHOST_SPMV_DOT) {
                        partsums[((pad+3*res->traits.ncols)*tid)+3*v+0] += 
                            lcol[row]*lcol[row];
                        partsums[((pad+3*res->traits.ncols)*tid)+3*v+1] += 
                            rcol[row]*lcol[row];
                        partsums[((pad+3*res->traits.ncols)*tid)+3*v+2] += 
                            rcol[row]*rcol[row];
                    }
                }
            }
        }
    }
    if (traits.flags & GHOST_SPMV_DOT) {
        if (!local_dot_product) {
            ERROR_LOG("The location of the local dot products is NULL!");
            return GHOST_ERR_INVALID_ARG;
        }
        for (v=0; v<res->traits.ncols; v++) {
            local_dot_product[v                       ] = 0.; 
            local_dot_product[v  +   res->traits.ncols] = 0.;
            local_dot_product[v  + 2*res->traits.ncols] = 0.;
            for (i=0; i<nthreads; i++) {
                local_dot_product[v                      ] += 
                    partsums[(pad+3*res->traits.ncols)*i + 3*v + 0];
                local_dot_product[v +   res->traits.ncols] += 
                    partsums[(pad+3*res->traits.ncols)*i + 3*v + 1];
                local_dot_product[v + 2*res->traits.ncols] += 
                    partsums[(pad+3*res->traits.ncols)*i + 3*v + 2];
            }
        }
        free(partsums);
    }

    GHOST_FUNC_EXIT(GHOST_FUNCTYPE_MATH|GHOST_FUNCTYPE_KERNEL);
    return GHOST_SUCCESS;
}

ghost_error ghost_sellspmv__a_plain_z_z_cm_CHUNKHEIGHT_NVECS(ghost_densemat *res, ghost_sparsemat *mat, ghost_densemat* invec, ghost_spmv_opts traits)
{
    GHOST_FUNC_ENTER(GHOST_FUNCTYPE_MATH|GHOST_FUNCTYPE_KERNEL);

    complex double *mval = (complex double *)SELL(mat)->val;
    complex double *local_dot_product = NULL;
    complex double *partsums = NULL;
    int nthreads = 1, i;
    ghost_lidx v;
   
    complex double *shift = NULL, scale = 1., beta = 1.; 
    complex double delta = 0., eta = 0.;
    ghost_densemat *z = NULL;
    unsigned clsize;
    ghost_machine_cacheline_size(&clsize);
    int pad = (int) clsize;///sizeof(complex double);

    GHOST_SPMV_PARSE_TRAITS(traits,scale,beta,shift,local_dot_product,z,delta,eta,complex double,
            complex double);

    if (traits.flags & GHOST_SPMV_DOT) {
#pragma omp parallel
        nthreads = ghost_omp_nthread();

        GHOST_CALL_RETURN(ghost_malloc((void **)&partsums,
                    (3*res->traits.ncols+pad)*nthreads*sizeof(complex double))); 
        for (i=0; i<(3*res->traits.ncols+pad)*nthreads; i++) {
            partsums[i] = 0.;
        }
    }
   
#pragma omp parallel shared (partsums) private(v)
    {
        ghost_lidx j,c,r,row;
        ghost_lidx offs;
        complex double tmp[CHUNKHEIGHT];
        complex double *rhsval = (complex double *)invec->val;
        int tid = ghost_omp_threadnum();

#pragma omp for schedule(runtime)
        for (c=0; c<mat->nrowsPadded/CHUNKHEIGHT; c++) 
        { // loop over chunks
            offs = SELL(mat)->chunkStart[c];
            
            for (v=0; v<NVECS; v++) {
                memset(tmp,0,sizeof(tmp));
                complex double *lcol = ((complex double *)(res->val))+res->stride*v;
                complex double *rcol = rhsval+invec->stride*v;
                complex double *zcol = NULL;
                if (z) { 
                    zcol = (complex double *)z->val+z->stride*v;
                }

                for (j=0; j<SELL(mat)->chunkLen[c]; j++) { // loop inside chunk
#pragma simd
                    for (r=0; r<CHUNKHEIGHT; r++) {
                        complex double matval = mval[offs+j*CHUNKHEIGHT+r];
                        ghost_lidx matcol = SELL(mat)->col[offs+j*CHUNKHEIGHT+r];
                        tmp[r] += matval*rcol[matcol];
                    }
                }

#pragma simd
                for (r=0; r<CHUNKHEIGHT; r++) {
                    row = c*CHUNKHEIGHT+r;
                    
                    if ((traits.flags & GHOST_SPMV_SHIFT) && shift) {
                        tmp[r] = tmp[r]-shift[0]*rcol[row];
                    }
                    if ((traits.flags & GHOST_SPMV_VSHIFT) && shift) {
                        tmp[r] = tmp[r]-shift[v]*rcol[row];
                    }
                    if (traits.flags & GHOST_SPMV_SCALE) {
                        tmp[r] = tmp[r]*scale;
                    }
                    if (traits.flags & GHOST_SPMV_AXPY) {
                        lcol[row] += tmp[r];
                    } else if (traits.flags & GHOST_SPMV_AXPBY) {
                        lcol[row] = beta*lcol[row] + tmp[r];
                    } else {
                        lcol[row] = tmp[r];
                    }
                    if (traits.flags & GHOST_SPMV_CHAIN_AXPBY) {
                        zcol[row] = delta*zcol[row] + eta*lcol[row];
                    }
                    
                    if (traits.flags & GHOST_SPMV_DOT) {
                        partsums[((pad+3*res->traits.ncols)*tid)+3*v+0] += 
                            conj(lcol[row])*lcol[row];
                        partsums[((pad+3*res->traits.ncols)*tid)+3*v+1] += 
                            conj(rcol[row])*lcol[row];
                        partsums[((pad+3*res->traits.ncols)*tid)+3*v+2] += 
                            conj(rcol[row])*rcol[row];
                    }
                }
            }
        }
    }
    if (traits.flags & GHOST_SPMV_DOT) {
        if (!local_dot_product) {
            ERROR_LOG("The location of the local dot products is NULL!");
            return GHOST_ERR_INVALID_ARG;
        }
        for (v=0; v<res->traits.ncols; v++) {
            local_dot_product[v                       ] = 0.; 
            local_dot_product[v  +   res->traits.ncols] = 0.;
            local_dot_product[v  + 2*res->traits.ncols] = 0.;
            for (i=0; i<nthreads; i++) {
                local_dot_product[v                      ] += 
                    partsums[(pad+3*res->traits.ncols)*i + 3*v + 0];
                local_dot_product[v +   res->traits.ncols] += 
                    partsums[(pad+3*res->traits.ncols)*i + 3*v + 1];
                local_dot_product[v + 2*res->traits.ncols] += 
                    partsums[(pad+3*res->traits.ncols)*i + 3*v + 2];
            }
        }
        free(partsums);
    }

    GHOST_FUNC_EXIT(GHOST_FUNCTYPE_MATH|GHOST_FUNCTYPE_KERNEL);
    return GHOST_SUCCESS;
}

ghost_error ghost_sellspmv__a_plain_z_z_rm_CHUNKHEIGHT_NVECS(ghost_densemat *res, ghost_sparsemat *mat, ghost_densemat* invec, ghost_spmv_opts traits)
{
    GHOST_FUNC_ENTER(GHOST_FUNCTYPE_MATH|GHOST_FUNCTYPE_KERNEL);

    complex double *mval = (complex double *)SELL(mat)->val;
    complex double *local_dot_product = NULL;
    complex double *partsums = NULL;
    int nthreads = 1, i;
    ghost_lidx v;
   
    complex double *shift = NULL, scale = 1., beta = 1.; 
    complex double delta = 0., eta = 0.;
    ghost_densemat *z = NULL;
    unsigned clsize;
    ghost_machine_cacheline_size(&clsize);
    int pad = (int) clsize/sizeof(complex double);

    GHOST_SPMV_PARSE_TRAITS(traits,scale,beta,shift,local_dot_product,z,delta,eta,complex double,
            complex double);

    if (traits.flags & GHOST_SPMV_DOT) {
#pragma omp parallel
        nthreads = ghost_omp_nthread();

        GHOST_CALL_RETURN(ghost_malloc((void **)&partsums,
                   (3*NVECS+pad)*nthreads*sizeof(complex double))); 
        for (i=0; i<(3*NVECS+pad)*nthreads; i++) {
            partsums[i] = 0.;
        }
    }
   
#pragma omp parallel shared (partsums) private(v)
    {
        ghost_lidx j,c,r,row;
        ghost_lidx offs;
        complex double tmp[CHUNKHEIGHT][NVECS];
        complex double *rhsval = (complex double *)invec->val;
        complex double *zval = NULL;
        if (z) {
            zval = (complex double *)z->val;
        }
        int tid = ghost_omp_threadnum();

#pragma omp for schedule(runtime)
        for (c=0; c<mat->nrowsPadded/CHUNKHEIGHT; c++) 
        { // loop over chunks
            offs = SELL(mat)->chunkStart[c];
                    
            memset(tmp,0,sizeof(tmp));
            for (j=0; j<SELL(mat)->chunkLen[c]; j++) { // loop inside chunk
                for (r=0; r<CHUNKHEIGHT; r++) {
#pragma simd
                    for (v=0; v<NVECS; v++) {
                        row = c*CHUNKHEIGHT+r;
                        complex double matval = mval[offs+j*CHUNKHEIGHT+r];
                        ghost_lidx matcol = SELL(mat)->col[offs+j*CHUNKHEIGHT+r];
                        tmp[r][v] += matval*rhsval[invec->stride*matcol+v];
                    }
                }
            }

            for (r=0; (r<CHUNKHEIGHT) && (c*CHUNKHEIGHT+r < mat->nrows); r++) {
                row = c*CHUNKHEIGHT+r;
                complex double *lrow = ((complex double *)(res->val))+res->stride*row;
                complex double *rrow = rhsval+invec->stride*row;
                complex double *zrow = NULL;
                if (z) {
                    zrow = zval+z->stride*row;
                }
                if ((traits.flags & GHOST_SPMV_SHIFT) && shift) {
#pragma simd
                    for (v=0; v<NVECS; v++) {
                        tmp[r][v] = tmp[r][v]-shift[0]*rrow[v];
                    }
                }
                if ((traits.flags & GHOST_SPMV_VSHIFT) && shift) {
#pragma simd
                    for (v=0; v<NVECS; v++) {
                        tmp[r][v] = tmp[r][v]-shift[v]*rrow[v];
                    }
                }
                if (traits.flags & GHOST_SPMV_SCALE) {
#pragma simd
                    for (v=0; v<NVECS; v++) {
                        tmp[r][v] = tmp[r][v]*scale;
                    }
                }
                if (traits.flags & GHOST_SPMV_AXPY) {
#pragma simd
                    for (v=0; v<NVECS; v++) {
                        lrow[v] += tmp[r][v];
                    }
                } else if (traits.flags & GHOST_SPMV_AXPBY) {
#pragma simd
                    for (v=0; v<NVECS; v++) {
                        lrow[v] = beta*lrow[v] + tmp[r][v];
                    }
                } else {
                    if (traits.flags & GHOST_SPMV_CHAIN_AXPBY) {
#pragma simd
                        for (v=0; v<NVECS; v++) {
                            lrow[v] = tmp[r][v];
                        }
                    } else {
#pragma vector nontemporal
#pragma simd
                        for (v=0; v<NVECS; v++) {
                            lrow[v] = tmp[r][v];
                        }
                    }
                }
                if (traits.flags & GHOST_SPMV_CHAIN_AXPBY) {
#pragma simd
                    for (v=0; v<NVECS; v++) {
                        zrow[v] = delta*zrow[v] + eta*lrow[v];
                    }
                }
                    
                if (traits.flags & GHOST_SPMV_DOT) {
#pragma simd
                    for (v=0; v<NVECS; v++) {
                        partsums[((pad+3*NVECS)*tid)+3*v+0] += 
                            conj(lrow[v])*lrow[v];
                        partsums[((pad+3*NVECS)*tid)+3*v+1] += 
                            conj(rrow[v])*lrow[v];
                        partsums[((pad+3*NVECS)*tid)+3*v+2] += 
                            conj(rrow[v])*rrow[v];
                    }
                }
            }
        }
    }
    if (traits.flags & GHOST_SPMV_DOT) {
        if (!local_dot_product) {
            ERROR_LOG("The location of the local dot products is NULL!");
            return GHOST_ERR_INVALID_ARG;
        }
        for (v=0; v<NVECS; v++) {
            local_dot_product[v                       ] = 0.; 
            local_dot_product[v  +   NVECS] = 0.;
            local_dot_product[v  + 2*NVECS] = 0.;
            for (i=0; i<nthreads; i++) {
                local_dot_product[v                      ] += 
                    partsums[(pad+3*NVECS)*i + 3*v + 0];
                local_dot_product[v +   NVECS] += 
                    partsums[(pad+3*NVECS)*i + 3*v + 1];
                local_dot_product[v + 2*NVECS] += 
                    partsums[(pad+3*NVECS)*i + 3*v + 2];
            }
        }
        free(partsums);
    }

    GHOST_FUNC_EXIT(GHOST_FUNCTYPE_MATH|GHOST_FUNCTYPE_KERNEL);
    return GHOST_SUCCESS;
}
